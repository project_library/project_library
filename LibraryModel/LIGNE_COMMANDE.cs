//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré à partir d'un modèle.
//
//     Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//     Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LibraryModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class LIGNE_COMMANDE
    {
        public int ID_LIVRE { get; set; }
        public byte[] DATE { get; set; }
        public decimal IDCOMMANDE { get; set; }
        public Nullable<int> QTE { get; set; }
    
        public virtual COMMANDE COMMANDE { get; set; }
        public virtual LIVRE LIVRE { get; set; }
    }
}
