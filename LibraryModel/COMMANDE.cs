//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré à partir d'un modèle.
//
//     Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//     Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LibraryModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class COMMANDE
    {
        public COMMANDE()
        {
            this.FACTUREs = new HashSet<FACTURE>();
            this.LIGNE_COMMANDE = new HashSet<LIGNE_COMMANDE>();
        }
    
        public decimal IDCOMMANDE { get; set; }
        public Nullable<decimal> IDFACTURE { get; set; }
        public decimal IDCLIENT { get; set; }
        public decimal IDADRESSE { get; set; }
        public Nullable<decimal> FRAISDEPORT { get; set; }
        public Nullable<int> DELAIDELIVRAISON { get; set; }
    
        public virtual ADRESSE ADRESSE { get; set; }
        public virtual CLIENT CLIENT { get; set; }
        public virtual FACTURE FACTURE { get; set; }
        public virtual ICollection<FACTURE> FACTUREs { get; set; }
        public virtual ICollection<LIGNE_COMMANDE> LIGNE_COMMANDE { get; set; }
    }
}
