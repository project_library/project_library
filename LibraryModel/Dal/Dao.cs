﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryModel
{
    public class Dao
    {
        #region Auteurs
        /// <summary>
        /// Permet de lister tous les Auteurs de la BDD 
        /// </summary>
        /// <returns>La liste de tous les Auteurs</returns>
        public List<Auteur> AllAuteurs()
        {
            using (var context = new LibraryModel())
            {
                return context.Auteurs == null ? null : context.Auteurs.ToList();
            }
        }

        /// <summary>
        /// Permet de lister les étudiants dont l'une des proprietes contient le parametre key
        /// </summary>
        /// <param name="key">Le tag de recherche</param>
        /// <returns>Liste des étudiants </returns>
        public List<Etudiant> AllEtudiantsByKey(string key)
        {
            using (var context = new UtopiaContext())
            {
                return context.Etudiants
                              .Where(
                                        e =>
                                            e.Id.ToString().Contains(key) ||
                                            e.Nom.Contains(key) ||
                                            e.Prenom.Contains(key)).ToList();
            }
        }

        /// <summary>
        /// Permet de rechercher un etudiant a partir de son nom et prenom
        /// </summary>
        /// <param name="nom">nom à rechercher</param>
        /// <param name="prenom">prenom à rechercher</param>
        /// <returns>Etudiant trouvé sinon null</returns>
        public Etudiant FindEtudiant(string nom, string prenom)
        {
            using (var context = new UtopiaContext())
            {
                Etudiant etudiant = context.Etudiants.FirstOrDefault(e => e.Nom == nom && e.Prenom == prenom);
                return etudiant;
            }
        }

        /// <summary>
        /// Permet d'ajouter un étudiant dans la BDD
        /// </summary>
        /// <param name="etudiant">Létudiant à ajouter</param>
        public void AjouterEtudiant(Etudiant etudiant)
        {
            using (var context = new UtopiaContext())
            {
                context.Entry(etudiant).State = EntityState.Added;
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Permet de modifier le nom et prenom d'un étudiant de la BDD
        /// </summary>
        /// <param name="etudiant">Etudiantt à modifier</param>
        public void ModifierEtudiant(Etudiant etudiant)
        {
            using (var context = new UtopiaContext())
            {
                context.Entry(etudiant).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Permet de supprimer un étudiant de la BDD
        /// </summary>
        /// <param name="etudiant">Etudiantt à supprimer</param>
        public void SupprimerEtudiant(Etudiant etudiant)
        {
            using (var context = new UtopiaContext())
            {
                context.Entry(etudiant).State = EntityState.Deleted;
                context.SaveChanges();
            }
        }
        #endregion


   /*    #region Matières
        /// <summary>
        /// Permet de lister toutes les matieres de la BDD
        /// </summary>
        /// <returns>Liste des matieres</returns>
        public List<Matiere> AllMatieres()
        {
            using (var context = new UtopiaContext())
            {
                try
                {
                    return context.Matieres.ToList();
                }
                catch (Exception)
                {
                    throw new Exception("La liste des matieres ext vide !!!");
                }
            }
        }

        /// <summary>
        /// Permet de lister les matieres dont l'une des proprietes contient le mot Key
        /// </summary>
        /// <param name="key">Le tag de recherche</param>
        /// <returns>Liste des matieres trouvées</returns>
        public List<Matiere> AllMatieresByKey(string key)
        {
            using (var context = new UtopiaContext())
            {
                return context.Matieres.Where(m => m.Id.ToString().Contains(key) || m.Titre.Contains(key)).ToList();
            }
        }

        /// <summary>
        /// Permet de rechercher une matiere à partir de son titre
        /// </summary>
        /// <param name="titre">Intitulé de la matiere</param>
        /// <returns>La matiere trouvé</returns>
        public Matiere FindMatiere(string titre)
        {
            using (var context = new UtopiaContext())
            {
                return context.Matieres.FirstOrDefault(m => m.Titre == titre);
            }
        }

        /// <summary>
        /// Permet de rechercher une matiere à partir de son id
        /// </summary>
        /// <param name="titre">Id de la matiere</param>
        /// <returns>La matiere trouvé</returns>
        public Matiere FindMatiere(int id)
        {
            using (var context = new UtopiaContext())
            {
                return context.Matieres.FirstOrDefault(m => m.Id == id);
            }
        }

        /// <summary>
        /// Permet de persister une matiere dans la BDD
        /// </summary>
        /// <param name="matiere">La matiere à persister</param>
        public void AjouterMatiere(Matiere matiere)
        {
            using (var context = new UtopiaContext())
            {
                context.Entry(matiere).State = EntityState.Added;
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Permet de supprimer une matiere de la BDD
        /// </summary>
        /// <param name="matiere">La matiere à supprimer</param>
        public void SupprimerMatiere(Matiere matiere)
        {
            using (var context = new UtopiaContext())
            {
                context.Entry(matiere).State = EntityState.Deleted;
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Permet de modifier une matiere de la BDD
        /// </summary>
        /// <param name="matiere">La matiere à modifier</param>
        public void ModifierMatiere(Matiere matiere)
        {
            using (var context = new UtopiaContext())
            {
                context.Entry(matiere).State = EntityState.Modified;
                context.SaveChanges();
            }
        }
        #endregion

        /// <summary>
        /// Permet de rechercher les inscriptions d'un etudiant
        /// </summary>
        /// <param name="etudiant">Le tag de la recherche</param>
        /// <returns>Listes d'inscription</returns>
        public List<Inscription> AllInscriptionsByEtudiant(Etudiant etudiant)
        {
            using (var context = new UtopiaContext())
            {
                var query = context.Inscriptions.Where(i => i.EtudiantId == etudiant.Id);//.Include("Etudiant").Include("Matiere");
                return query.ToList();
            }
        } 
    }*/
}
